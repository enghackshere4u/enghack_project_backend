* This is a simple server side web api.
* The api uses a Postgres database and the python Flask Framework.

## Installation
* To run this on your computer you must first install [docker](https://docs.docker.com/engine/installation/).
* Once Docker is installed, run the following command: `cd app && docker-compose up --build -d`
* The application will be ready to use in `localhost:5000` port

## Running
* After the application has been installed, use this commands to run and stop the application.
* Upon start, a data base initialization sequence will seed sample data in the db, so the endpoints
can be tested immediately after.
```c 
	docker-compose up --build -d   # Run the container.

	docker-compose down   # Stop and remove everything.
```