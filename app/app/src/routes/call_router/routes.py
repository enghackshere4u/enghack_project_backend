import json
import base64
from pydub import AudioSegment
from src.translator.translator import translateAudioFile, languageTranslate
from src.handlers.message_history import store_message, get_conversation_history, get_message_existence, get_conversation_history_by_id

from . import call_router
from flask import Flask, redirect, request, Response


@call_router.route('/call_router', methods=['POST'])
def translate():
    payload_binary = request.get_json()['payload'].encode()

    with open("Output.m4a", "wb") as fh:
        fh.write(base64.decodebytes(payload_binary))

    AudioSegment.from_file("Output.m4a").export("Output.wav", format='wav')
    converted = translateAudioFile("Output.wav")
    error = store_message(
        request.get_json()['conversationID'],
        request.get_json()['userFrom'],
        request.get_json()['userTo'],
        converted,
        request.get_json()['payload']
    )
    if error is not None:
        return Response(json.dumps({"Error": str(error)}), status=500, mimetype='application/json')

    return Response(json.dumps({"Content": converted}), status=200, mimetype='application/json')


@call_router.route('/conversation_history', methods=['GET'])
def getConversationHistory():
    conversation_history = get_conversation_history()
    if not isinstance(conversation_history, list):
        return Response(json.dumps({"Error": str(conversation_history)}), status=500, mimetype='application/json')
    elif len(conversation_history) < 1:
        return Response(json.dumps({"conversationHistory": []}), status=200, mimetype='application/json')
    else:
        return Response(json.dumps({"conversationHistory": conversation_history}), status=200, mimetype='application/json')


@call_router.route('/get_message', methods=['GET'])
def get_message():
    user_to = request.args.get('userTo')
    language = request.args.get('language')
    existence = get_message_existence(user_to, language)
    if not isinstance(existence, list):
        return Response(json.dumps({"Error": str(existence)}), status=500, mimetype='application/json')
    elif len(existence) < 1:
        return Response(json.dumps({"existence": []}), status=200, mimetype='application/json')
    else:
        return Response(json.dumps({"existence": existence}), status=200, mimetype='application/json')


@call_router.route('/translate_message', methods=['POST'])
def translateMessage():
    if request.get_json()['originalMessage'] is None:
        return Response(json.dumps("Bad Request missing key originalMessage"), status=400, mimetype='application/json')

    if request.get_json()['translateToLanguage'] is None:
        return Response(json.dumps("Bad Request missing key translateToLanguage"), status=400,
                        mimetype='application/json')

    translation_response = languageTranslate(request.get_json()['originalMessage'],
                                             request.get_json()['translateToLanguage'])

    return Response(json.dumps(translation_response), status=200, mimetype='application/json')


@call_router.route('/translate_message', methods=['GET'])
def translateConversation():
    if request.args.get('conversationID', None) == 'true':
        return Response(json.dumps("Bad Request missing key conversationID"), status=400, mimetype='application/json')

    if request.args.get('language', None) == 'true':
        return Response(json.dumps("Bad Request missing key language"), status=400, mimetype='application/json')

    conversation_history = get_conversation_history_by_id(
        request.args.get('conversationID'),
        request.args.get('language')
    )

    if not isinstance(conversation_history, list):
        return Response(json.dumps({"Error": str(conversation_history)}), status=500, mimetype='application/json')
    elif len(conversation_history) < 1:
        return Response(json.dumps({"conversationHistory": []}), status=200, mimetype='application/json')
    else:
        return Response(json.dumps({"conversationHistory": conversation_history}), status=200, mimetype='application/json')

