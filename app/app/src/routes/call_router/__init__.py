# app/src/call_router/__init__.py

from flask import Blueprint

call_router = Blueprint('call_router', __name__)

from . import routes
