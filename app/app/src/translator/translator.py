import speech_recognition as sr
from googletrans import Translator
from src.models.translated_message import build_translated_message_object 

def translateAudioFile(audiofile):
    r = sr.Recognizer()
    input_wav = sr.AudioFile(audiofile)
    with input_wav as source:
        audio = r.record(source)
        text_from_speech = r.recognize_google(audio)
        return text_from_speech

def languageTranslate(original_message, target_language):
    translator = Translator()
    original_language = translator.detect(original_message).lang
    translated_message = translator.translate(original_message, target_language).text
    return build_translated_message_object(
            original_language,
            original_message,
            target_language,
            translated_message
        )