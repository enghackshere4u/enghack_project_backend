from src import db


def build_translated_message_object(original_langauge, original_message, translation_language, translated_message):
    return {
        "originalLanguage": original_langauge,
        "originalMessage": original_message,
        "translationLanguage": translation_language,
        "translatedMessage": translated_message
    }
