from src import db
from datetime import datetime


class message_history(db.Model):
    __table_args__ = (
    )
    c_id = db.Column(db.String(255), nullable=False)
    u_from = db.Column(db.Integer, nullable=False)
    u_to = db.Column(db.Integer, nullable=False)
    message = db.Column(db.Text, nullable=False)
    message_binary = db.Column(db.Text, nullable=False)
    time = db.Column(db.DateTime, default=datetime.utcnow, primary_key=True)
    message_read = db.Column(db.Integer, default=0)


def build_message_history_object(message_history, translated_message=""):
    return {
        "conversationID": message_history.c_id,
        "user_from": message_history.u_from,
        "user_to": message_history.u_to,
        "message": message_history.message,
        "translated_message": translated_message,
        "time": message_history.time.__str__(),
        "messageBinary": message_history.message_binary
    }

def build_message_history_object_text_only(message_history):
    return {
        "conversationID": message_history.c_id,
        "user_from": message_history.u_from,
        "user_to": message_history.u_to,
        "message": message_history.message,
        "time": message_history.time.__str__(),
    }