import uuid
from src import db
from sqlalchemy import and_
from src.models.message_history import message_history, build_message_history_object, \
    build_message_history_object_text_only
from src.translator.translator import languageTranslate


def store_message(conversation_id, user_from, user_to, message_string, m_binary):
    try:
        db.session.add(
            message_history(
                c_id=conversation_id,
                u_from=user_from,
                u_to=user_to,
                message=message_string,
                message_binary=m_binary
            )
        )
        db.session.commit()
        return
    except Exception as e:
        db.session.rollback()
        return e


def get_conversation_history():
    try:
        conversation_history = db.session.query(message_history).order_by(message_history.time.desc()).all()
        conversation_history_response = []
        for conversation in conversation_history:
            conversation_history_response.append(build_message_history_object_text_only(conversation))
        return conversation_history_response
    except Exception as e:
        return e


def get_conversation_history_by_id(conversation_id, language_id=''):
    try:
        conversation_history = db.session.query(
            message_history
        ).filter(
            message_history.c_id == conversation_id
        ).order_by(
            message_history.time.desc()
        ).all()
        conversation_history_response = []
        for conversation in conversation_history:
            if language_id:
                conversation.message = languageTranslate(conversation.message, language_id)
            conversation_history_response.append(build_message_history_object_text_only(conversation))
        return conversation_history_response
    except Exception as e:
        return e


def get_message_existence(user_to, language_id=''):
    try:
        conversation_history = db.session.query(message_history). \
            filter((and_(message_history.message_read == 0, message_history.u_to == user_to))). \
            order_by((message_history.time.asc())).all()
        conversation_history_response = []
        for conversation in conversation_history:
            if language_id:
                translated_message = languageTranslate(conversation.message, language_id)[
                    'translatedMessage']
                conversation_history_response.append(build_message_history_object(conversation, translated_message))
            else:
                conversation_history_response.append(build_message_history_object(conversation))
            conversation.message_read = 1
        try:
            db.session.commit()
        except Exception as e:
            db.session.rollback()
            return e
        return conversation_history_response
    except Exception as e:
        return e
