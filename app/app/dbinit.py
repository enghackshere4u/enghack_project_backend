import time
from src import db
from src.models.message_history import message_history

def database_initialization_sequence():

    db.session.add(
        message_history()
    )
    try:
        db.session.commit()
    except Exception:
        db.session.rollback()
